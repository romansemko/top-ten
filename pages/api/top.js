import axios from "axios";

// Yes, I DID commit the API key. =)
const API_KEY = "e11de9d4-758e-4563-b94f-cf48c6c15878";

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { collection_address } = req.body;

    const url = `https://api.traitsniper.com/v1/collections/${collection_address}/ranks?sort_rank=desc&limit=10`;
    const headers = { "x-ts-api-key": API_KEY };
    const response = await axios.get(url, { headers });

    // Query for each token ID API to get the most recent TXs
    // Add that data to each token object

    res.status(200).json(response.data);
  } else {
    res.setHeader("Allow", "POST");
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
