import axios from "axios";
import { useState } from "react";

export default function Ten() {
  const [collectionAddress, setCollectionAddress] = useState("");
  const [nfts, setNfts] = useState([]);
  const [error, setError] = useState("");

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const response = await axios.post("/api/top", {
        collection_address: collectionAddress,
      });
      setNfts(response.data.ranks);
      setError("");
    } catch (error) {
      setError("Error fetching NFTs. Please try again later.");
      setNfts([]);
    }
  }

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-4xl font-bold mb-4">Top 10 rare NFTs</h1>
      <form onSubmit={handleSubmit} className="max-w-md w-full">
        <label htmlFor="collectionAddress" className="block mb-2">
          Collection Address
        </label>
        <input
          type="text"
          required
          id="collectionAddress"
          value={collectionAddress}
          onChange={(e) => setCollectionAddress(e.target.value)}
          className="block w-full border border-gray-300 rounded-md px-4 py-2 mb-4"
        />
        <button
          type="submit"
          className="bg-blue-500 text-white py-2 px-4 rounded-md"
        >
          Get Top 10 rare NFTs!
        </button>
      </form>
      {error && <p className="text-red-600 mt-4">{error}</p>}
      <ul className="mt-10 space-y-2">
        {nfts.map((nft) => (
          <li key={nft.token_id}>
            Token ID: {nft.token_id} - Rank: {nft.rarity_rank}
          </li>
        ))}
      </ul>
    </div>
  );
}
